package com.stefanomantini.carcoststracker.api.consumer;

import com.stefanomantini.carcoststracker.model.Expense;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@Service
public class ExpenseConsumer {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExpenseConsumer.class);

  private final CountDownLatch latch = new CountDownLatch(1);

  public CountDownLatch getLatch() {
    return latch;
  }

  /**
   * TODO needs to use a stream
   *
   * @param payload
   */
  @KafkaListener(topics = "${kafka.expense.topic}")
  public void receive(final String payload) {
    LOGGER.info("received payload='{}'", payload);
    latch.countDown();
  }

  /**
   * TODO use a ktable - see: DeliveryNotificationStream.java
   *
   * @return
   */
  public List<Expense> findAll() {
    return Arrays.asList();
  }
}
