package com.stefanomantini.carcoststracker.api.resource;

import com.stefanomantini.carcoststracker.api.consumer.ExpenseConsumer;
import com.stefanomantini.carcoststracker.model.Expense;
import com.stefanomantini.carcoststracker.service.producer.ExpenseProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ExpenseResource {

  @Autowired private ExpenseProducer expenseProducer;

  @Autowired private ExpenseConsumer expenseConsumer;

  @Value("${kafka.expense.topic}")
  private String topic;

  @GetMapping(value = "/expenses")
  public List<Expense> getExpenses() {
    return expenseConsumer.findAll();
  }

  @PostMapping(value = "/expenses")
  public void publishExpense(@RequestBody @Valid final Expense expense) {
    expenseProducer.send(expense);
  }
}
