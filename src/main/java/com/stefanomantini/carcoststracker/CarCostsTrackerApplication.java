package com.stefanomantini.carcoststracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// https://dzone.com/articles/magic-of-kafka-with-spring-boot
@SpringBootApplication
public class CarCostsTrackerApplication {

  public static void main(String[] args) {
    SpringApplication.run(CarCostsTrackerApplication.class, args);
  }
}
