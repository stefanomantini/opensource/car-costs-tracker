package com.stefanomantini.carcoststracker.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Expense {
  private String id;
  private String name;
  private Double price;
  private Long timeSubmitted;
  private Long timestamp;
  private String description;
}
