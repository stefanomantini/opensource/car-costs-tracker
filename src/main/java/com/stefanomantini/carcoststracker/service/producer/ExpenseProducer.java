package com.stefanomantini.carcoststracker.service.producer;

import com.stefanomantini.carcoststracker.model.Expense;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ExpenseProducer {

  private static final Logger LOG = LoggerFactory.getLogger(ExpenseProducer.class);

  @Autowired private KafkaTemplate<String, Expense> kafkaTemplate;

  @Value("${kafka.expense.topic}")
  private String topic;

  public void send(final Expense data) {
    LOG.info("sending data='{}' to topic='{}'", data, topic);

    kafkaTemplate.send(topic, data);
  }
}
