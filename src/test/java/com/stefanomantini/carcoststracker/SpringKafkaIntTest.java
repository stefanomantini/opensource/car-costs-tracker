package com.stefanomantini.carcoststracker;

import com.stefanomantini.carcoststracker.api.consumer.ExpenseConsumer;
import com.stefanomantini.carcoststracker.model.Expense;
import com.stefanomantini.carcoststracker.service.producer.ExpenseProducer;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringKafkaIntTest {

  @Autowired private ExpenseProducer sender;

  @Autowired private ExpenseConsumer receiver;

  @Autowired private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

  private static final String SENDER_TOPIC = "sender.t";

  @ClassRule
  public static EmbeddedKafkaRule embeddedKafka = new EmbeddedKafkaRule(1, true, SENDER_TOPIC);

  @Before
  public void setUp() throws Exception {
    // wait until the partitions are assigned
    for (final MessageListenerContainer messageListenerContainer :
        kafkaListenerEndpointRegistry.getListenerContainers()) {
      ContainerTestUtils.waitForAssignment(
          messageListenerContainer, embeddedKafka.getEmbeddedKafka().getPartitionsPerTopic());
    }
  }

  @Test
  public void testReceive() throws Exception {
    sender.send(Expense.builder().build());

    receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);

    assertThat(receiver.getLatch().getCount()).isEqualTo(0);
  }
}
