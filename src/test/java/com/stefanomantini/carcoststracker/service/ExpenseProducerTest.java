package com.stefanomantini.carcoststracker.service;

import com.stefanomantini.carcoststracker.model.Expense;
import com.stefanomantini.carcoststracker.service.producer.ExpenseProducer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

// https://github.com/code-not-found/spring-kafka/blob/master/spring-kafka-test-single-embedded/src/test/java/com/codenotfound/kafka/producer/SpringKafkaSenderTest.java
@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class ExpenseProducerTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExpenseProducerTest.class);

  @Autowired private ExpenseProducer sender;

  private static final String SENDER_TOPIC = "sender.t";

  private KafkaMessageListenerContainer<String, String> container;

  private BlockingQueue<ConsumerRecord<String, Expense>> records;

  @ClassRule
  public static EmbeddedKafkaRule embeddedKafka = new EmbeddedKafkaRule(1, true, SENDER_TOPIC);

  @Before
  public void setUp() {
    final Map<String, Object> consumerProperties =
        KafkaTestUtils.consumerProps("sender", "false", embeddedKafka.getEmbeddedKafka());

    final DefaultKafkaConsumerFactory<String, String> consumerFactory =
        new DefaultKafkaConsumerFactory<>(consumerProperties);

    final ContainerProperties containerProperties = new ContainerProperties(SENDER_TOPIC);

    container = new KafkaMessageListenerContainer<>(consumerFactory, containerProperties);

    records = new LinkedBlockingQueue<>();

    container.setupMessageListener(
        (MessageListener<String, Expense>)
            record -> {
              LOGGER.debug("test-listener received message='{}'", record.toString());
              records.add(record);
            });

    container.start();

    ContainerTestUtils.waitForAssignment(
        container, embeddedKafka.getEmbeddedKafka().getPartitionsPerTopic());
  }

  @After
  public void tearDown() {
    container.stop();
  }

  @Test
  public void testSend() throws InterruptedException {
    final Expense expenseRecord =
        Expense.builder()
            .id(UUID.randomUUID().toString())
            .name("Fuel")
            .price(Double.MAX_VALUE)
            .timestamp(Instant.now().toEpochMilli())
            .timeSubmitted(Instant.now().minusSeconds(999).toEpochMilli())
            .build();

    sender.send(expenseRecord);

    final ConsumerRecord<String, Expense> received = records.poll(2, TimeUnit.SECONDS);

    assertEquals(expenseRecord, received);
  }
}
